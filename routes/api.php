<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth', 'namespace' => 'App\\Api\\V1\\Controllers\\'], function (Router $api) {
        $api->post('signup', 'SignUpController@signUp');
        $api->post('login', 'LoginController@login');

        $api->post('recovery', 'ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'ResetPasswordController@resetPassword');

        $api->post('logout', 'LogoutController@logout');
        $api->post('refresh', 'RefreshController@refresh');
        $api->get('me', 'UserController@me');
    });

    $api->group(['middleware' => 'jwt.auth'], function (Router $api) {
        $api->get('protected', 'App\\Api\\V1\\Controllers\\DummyController@protected');

        $api->group(['middleware' => 'jwt.refresh'], function (Router $api) {
            $api->get('refresh', 'App\\Api\\V1\\Controllers\\DummyController@refresh');
        });
    });

    $api->get('hello', 'App\\Api\\V1\\Controllers\\DummyController@hello');
    
    // PDF for everyone
    $api->post('pdf', 'App\\Api\\V1\\Controllers\\PDFUploadController@index');
});
