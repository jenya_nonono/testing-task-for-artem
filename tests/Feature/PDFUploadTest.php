<?php

namespace App\Feature;

use App\TestCase;
use Illuminate\Http\UploadedFile;


class PDFUploadTest extends TestCase
{
    /**
     * Why not, but fake return empty exeption, need use real filesystem.
     *
     * @return void
     */
    public function testUpload()
    {
        $sizeInKilobytes = 400;
        $response = $this->json('POST', '/api/pdf', [
            'attachment' => [
                UploadedFile::fake()->create('document1.pdf', $sizeInKilobytes),
                UploadedFile::fake()->create('document2.pdf', $sizeInKilobytes),
                UploadedFile::fake()->create('document3.pdf', $sizeInKilobytes)
            ]
        ]);

        $response->assertStatus(200);
    }
}