<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Api\V1\Requests\PDFUploadRequest;
use LynX39\LaraPdfMerger\Facades\PdfMerger;

class PDFUploadController extends Controller
{    
    public function index(PDFUploadRequest $request)
    {
        $pdfMergeInstance = PdfMerger::init();
        foreach ($request->file('attachment') as $file) {
            if($file->getMimeType() == 'application/pdf')
            {
                $pdfMergeInstance->addPDF($file->getPathname(), 'all');       
            }
        }
        $pdfMergeInstance->merge();
        
        $FileName = 'attachment_'.md5(rand(0,231));
        $pdfMergeInstance->save(public_path('/upload/' . $FileName . '.pdf'));

        return response()->json(['message' => "Created new " . $FileName], 201);
    }
}
