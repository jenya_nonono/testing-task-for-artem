<?php


namespace App\Api\V1\Controllers;


use App\Http\Controllers\Controller;

class DummyController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function hello()
    {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return response()->json([
            'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse\
     */
    public function protected()
    {
        return response()->json([
            'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        return view('welcome');
    }
}