Hey there! Welcome to Sys Softwaresysteme GmbH test task!

[Here is](https://github.com/francescomalatesta/laravel-api-boilerplate-jwt) boilerplate for laravel with plugins.

## Task:
We need to create simple api call which will apply array of file names. If all of the url's are pdf we need to merge them in one.
Otherwise ignore.
Output should be stored to some `/tmp` path and being outputted.

Please review boilerplate and be ready to say about pros and cons. 
Like a big plus will be preparing theses about way of right app architecture for restful api. We have to discuss them right with reviewing of test task.
Will be great to hear also about your review of RESTful API. how that's should be implemented.


Thanks and good luck!

Best Regards,

Artem   
